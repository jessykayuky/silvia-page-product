$(document).ready(function() {

    // smooth scroll

    $("a").on('click', function(event) {
        if (this.hash !== "") {

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top - 120
            }, 800, function() {
                window.location.hash = hash;
            });
        }
    });

    // menu lateral 

    $(".close-container").click(function() {
        $(this).toggleClass('active');
        $('aside').toggleClass('active');
        $('aside').find('.submenu').removeClass('active');
        $('.overlay').toggleClass('active');
    });

    $("aside .menu-lateral ul li a").mouseenter(function() {
        var submenu = $(this).attr('data');
        $('aside').find('#' + submenu).addClass('active');
        $('aside').find('#' + submenu).siblings().removeClass('active');
    });

    $("aside").mouseleave(function() {
        $('aside').find('.submenu').removeClass('active');
        $('.close-container').removeClass('active');
        $('aside').removeClass('active');
        $('.overlay').toggleClass('active');
    });

    // voltar menu lateral no mobile

    $(".voltar").click(function() {
        $('aside').find('.submenu').removeClass('active');
    });

    // search 

    $("header .wrap-menu .right-options .options .search").mouseenter(function() {
        $(this).find('input').addClass('active');
    });

    $("header .wrap-menu").mouseleave(function() {
        $(this).find('input').removeClass('active');
    });

    //fixa o menu on scroll

    $(window).scroll(function() {
        if ($(window).scrollTop() > 10) {
            $('header').addClass('fixed').removeClass('unfixed');
        } else {
            $('header').addClass('unfixed').removeClass('fixed');
        }
    });

    // slick slider

    $('.prateleira ul').slick({
        infinite: true,
        autoplay: false,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    // responsividade especifica

    var isMobile = $(window).width() <= 768,
        isTablet = $(window).width() <= 998,
        isDesktop = $(window).width() >= 1024,
        isDesktopMed = $(window).width() >= 1200,
        isDesktopMax = $(window).width() >= 1400;

    if (isMobile) {

    }

    if (isDesktop) {

        // inicia o wow.js
        new WOW().init();
    }

});